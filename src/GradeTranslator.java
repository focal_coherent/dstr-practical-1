import java.util.Scanner;

public class GradeTranslator {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int score = 0;
        char grade;

        System.out.print("Enter an integer: ");
        score = sc.nextInt();

        if (score >= 80) {
            grade = 'A';
        }
        else if (score >= 70) {
            grade = 'B';
        }
        else if (score >= 60) {
            grade = 'C';
        }
        else if (score >= 50) {
            grade = 'D';
        }
        else {
            grade = 'F';
        }

        System.out.println("Mark: " + score + ", Grade: " + grade);
    }
    
}