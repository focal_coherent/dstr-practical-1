public class Forever {
    
    public static void main(String[] args) {
        int count = 20;

        while (count >= 0) {
            System.out.println(count);
            /* count = count + 1; */ // infinite loop
            count = count - 1;
        }
    }
        
}