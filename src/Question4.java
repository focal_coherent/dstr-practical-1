import java.util.Scanner;

public class Question4 {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double balance, interest, expense;
        
        System.out.print("Enter starting account balance (S$): ");
        balance = sc.nextDouble();
        System.out.print("Enter compund interest per month (%): ");
        interest = sc.nextDouble();
        System.out.print("Enter expense per month (S$): ");
        expense = sc.nextDouble();

        int noOfMonths = depletedIn(balance, interest, expense);
        System.out.printf("The account will be completely depleted in %d months.", noOfMonths);

    }

    public static int depletedIn(double balance, double interest, double expense) {
        int ret = 0;
        while (balance > 0) { // until account is depleted,
            balance *= (1 + interest / 100); // compund interest(%) for each month
            balance -= expense; // and subtract expense for each month
            ret += 1; // increment the number of months taken for account to be depleted
        }

        return ret;
    }
}